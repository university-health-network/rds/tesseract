#!/bin/bash

DIR="$(git root)"

## build & push the containers, then tell HELM to deploy

($DIR/containers/build_and_push.sh) && ($DIR/tesseract/deploy.sh)
