import { Component, OnInit } from '@angular/core';
import { sections, debug } from '../globals';
import { StateEngine } from '../stateEngine.service';

@Component({
	selector: 'top-nav',
	templateUrl: './top-nav.component.html',
	styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {
	sections = sections;
	debug = debug;
	hamState: number;

	// tslint:disable-next-line: no-shadowed-variable
	constructor(public stateEngine: StateEngine) {
		this.stateEngine.hamburgerState.subscribe(n => {
			this.hamState = n;
		});
	}

	toggleSideNav() {
		this.stateEngine.toggleHamburger();
		return false;
	}

	ngOnInit() { }
}
