import { glue, debug } from '../environments/environment';

const sections = [
	['home', 'Home'],
	['devices', 'Devices'],
	['projects', 'Projects'],
	['users', 'Users'],
	['roles', 'Roles']
];

export { sections, glue, debug };
