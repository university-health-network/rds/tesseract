import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { ApiService } from "../../api.service";
import { HighlightService } from '../../prism.service';

@Component({
	selector: 'app-buckets',
	templateUrl: './buckets.component.html',
	styleUrls: ['../../flexbox.component.css']
})
export class BucketsComponent implements OnInit, AfterViewChecked {

	deviceBuckets = [];
	highlighted = false;

	constructor(private api: ApiService, private prism: HighlightService) { }

	async getData() {
		return this.api.GET('device-buckets').toPromise();
	}

	ngAfterViewChecked() {
		if (this.deviceBuckets && !this.highlighted) {
			this.prism.highlightAll();
			this.highlighted = true;
		}
	}

	ngOnInit() {
		this.getData().then(data => {
			this.deviceBuckets = data.payload;
		});
	}

}
