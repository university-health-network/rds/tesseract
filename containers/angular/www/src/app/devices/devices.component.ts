import { Component, OnInit } from '@angular/core';

import { TypesComponent } from './types/types.component';
import { InstancesComponent } from './instances/instances.component';
import { BucketsComponent } from './buckets/buckets.component';

@Component({
	selector: 'app-devices',
	templateUrl: './devices.component.html',
	styleUrls: ['../flexbox.component.css']
})
class RootComponent implements OnInit {

	constructor() { }
	navItems = [
		['', 'Devices'],
		['types', 'Device Types'],
		['instances', 'Device Instances'],
		['buckets', 'Device Buckets']
	];
	ngOnInit() { }

}

export {
	RootComponent,
	TypesComponent,
	InstancesComponent,
	BucketsComponent
};
