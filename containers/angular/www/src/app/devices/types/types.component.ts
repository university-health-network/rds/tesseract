import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { ApiService } from "../../api.service";
import { HighlightService } from '../../prism.service';

@Component({
	selector: 'app-types',
	templateUrl: './types.component.html',
	styleUrls: ['../../flexbox.component.css']
})
export class TypesComponent implements OnInit, AfterViewChecked {
	constructor(private api: ApiService, private prism: HighlightService) { }
	deviceTypes: any[];
	highlighted = false;
	async getData() {
		const deviceTypes = await this.api.GET('device-types').toPromise();
		return { deviceTypes };
	}
	ngOnInit() {
		this.getData().then(data => {
			this.deviceTypes = data.deviceTypes.payload;
		});
	}
	ngAfterViewChecked() {
		if (this.deviceTypes && !this.highlighted) {
			this.prism.highlightAll();
			this.highlighted = true;
		}
	}
}
