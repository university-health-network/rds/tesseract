import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { ApiService, ApiResponse } from "../../api.service";

import { HighlightService } from '../../prism.service';

@Component({
	selector: 'app-instances',
	templateUrl: './instances.component.html',
	styleUrls: ['../../flexbox.component.css']
})

export class InstancesComponent implements OnInit, AfterViewChecked {
	constructor(private api: ApiService, private prism: HighlightService) { }
	deviceTypes: object[];
	highlighted = false;
	async getData() {
		const [deviceTypes, deviceInstances] = await Promise.all([
			this.api.GET('device-types').toPromise(),
			this.api.GET('device-instances').toPromise()
		]);
		return { deviceTypes, deviceInstances };
	}
	ngOnInit() {
		this.getData().then(data => {
			const bigTypes = data.deviceTypes.payload.map(t => {
				const row = {
					deviceType: t['make'] + ' -> ' + t['model'],
					instances: data.deviceInstances.payload.filter(i => {
						return (i['deviceTypeId'] === t['id']);
					})
				};
				return row;
			});
			this.deviceTypes = bigTypes;
		});
	}
	ngAfterViewChecked() {
		if (this.deviceTypes && !this.highlighted) {
			this.prism.highlightAll();
			this.highlighted = true;
		}
	}
}
