import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debug } from '../globals';

import { StateEngine } from '../stateEngine.service';

@Component({
	selector: 'side-nav',
	templateUrl: './side-nav.component.html',
	styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

	debug = debug;
	hamState: number;

	constructor(private route: ActivatedRoute, public stateEngine: StateEngine) {
		this.stateEngine.hamburgerState.subscribe(x => {
			this.hamState = x;
		});
	}

	@Input() basePath: string;
	@Input() navItems: object;

	cleanse(arr: string[]): string {
		return arr.filter(a => a).join('/');
	}

	ngOnInit() { }

}
