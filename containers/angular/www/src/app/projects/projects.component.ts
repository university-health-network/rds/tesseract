import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-projects',
	templateUrl: './projects.component.html',
	styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

	constructor() { }
	navItems = [
		['home', 'Projects'],
		['buckets', 'Project Buckets']
	];
	ngOnInit() {
	}

}
