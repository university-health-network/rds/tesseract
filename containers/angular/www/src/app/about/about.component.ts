import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OrgComponent } from './org/org.component';
import { TeamComponent } from './team/team.component';
import { PizzaComponent } from './pizza/pizza.component';
import { HomeComponent } from './home/home.component';

@Component({
	selector: 'app-about',
	templateUrl: './about.component.html',
	styleUrls: ['./about.component.css']
})
class MainComponent implements OnInit {
	constructor(private route: ActivatedRoute) { }
	navItems = [
		['home', 'About'],
		['team', 'The RDS Team'],
		['org', 'The Organization'],
		['pizza', 'Facts about Pizza']
	];
	ngOnInit() { }
}

export {
	MainComponent,
	OrgComponent,
	TeamComponent,
	PizzaComponent,
	HomeComponent
};
