import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { glue } from './globals';

interface ApiResponse {
	"_": object;
	"payload": object[];
}

@Injectable({
	providedIn: 'root'
})
class ApiService {
	baseUrl = glue.restserver.endpoint;
	constructor(private httpClient: HttpClient) { }
	get_long() {
		return this.httpClient.get(this.baseUrl + '/long');
	}
	GET(what: string) {
		return this.httpClient.get<ApiResponse>(this.baseUrl + '/' + what);
	}
}

export { ApiService, ApiResponse };
