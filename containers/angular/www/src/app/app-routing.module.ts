import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 	routes
import { HomeComponent } from './home/home.component';
// import * as About from './about/about.component';
import * as Devices from './devices/devices.component';
import { ProjectsComponent } from './projects/projects.component';
import { UsersComponent } from './users/users.component';
// import { RolesComponent } from './roles/roles.component';

const routes: Routes = [
	{ "path": "", redirectTo: "/home", pathMatch: 'full' },
	{ "path": "home", component: HomeComponent },
	{
		"path": "devices", component: Devices.RootComponent, children: [
			{
				"path": "types",
				"component": Devices.TypesComponent
			},
			{
				"path": "instances",
				"component": Devices.InstancesComponent
			},
			{
				"path": "buckets",
				"component": Devices.BucketsComponent
			}
		]
	},
	{ "path": "projects", component: ProjectsComponent },
	{ "path": "users", component: UsersComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
