import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { TopNavComponent } from './top-nav/top-nav.component';

import { ProjectsComponent } from './projects/projects.component';
import { UsersComponent } from './users/users.component';
import { HomeComponent } from './home/home.component';

import * as About from './about/about.component';
import * as Devices from './devices/devices.component';

import { HttpClientModule } from '@angular/common/http';
import { TeamComponent } from './about/team/team.component';
import { OrgComponent } from './about/org/org.component';
import { PizzaComponent } from './about/pizza/pizza.component';
import { TypesComponent } from './devices/types/types.component';
import { InstancesComponent } from './devices/instances/instances.component';

import { HighlightService } from './prism.service';
import { BucketsComponent } from './devices/buckets/buckets.component';

@NgModule({
	declarations: [
		AppComponent,
		SideNavComponent,
		TopNavComponent,
		About.MainComponent,
		About.HomeComponent,
		Devices.RootComponent,
		Devices.TypesComponent,
		Devices.InstancesComponent,
		ProjectsComponent,
		UsersComponent,
		HomeComponent,
		TeamComponent,
		OrgComponent,
		PizzaComponent,
		TypesComponent,
		InstancesComponent,
		BucketsComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserAnimationsModule
	],
	providers: [HighlightService],
	bootstrap: [
		AppComponent
	]
})
export class AppModule { }
