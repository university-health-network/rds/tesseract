import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class StateEngine {
	hamburgerValue = 1;
	private hamburgerInitialState: BehaviorSubject<number> = new BehaviorSubject(this.hamburgerValue);
	hamburgerState = this.hamburgerInitialState.asObservable();
	constructor() { }
	toggleHamburger() {
		this.hamburgerValue = Math.abs(this.hamburgerValue - 1);
		this.hamburgerInitialState.next(this.hamburgerValue);
	}
}
