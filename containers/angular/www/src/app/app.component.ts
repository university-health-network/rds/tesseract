import { Component } from '@angular/core';
import { StateEngine } from './stateEngine.service';
import { ApiService } from './api.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent {
	private locations = [];
	constructor(public stateEngine: StateEngine, public api: ApiService) { }
	title = 'tesseract';
	hak = {
		"proj": "rds",
		"app": "tesseract",
		"org": "techna",
		"role": "frontend"
	};
}
