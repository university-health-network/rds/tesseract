const glue = {
	"restserver": {
		"endpoint": window.location.protocol + '//' + window.location.host.replace('-web', '-api'),
		"api": {
			"version": "v1.0.1"
		}
	}
};

const debug = false;

// tslint:disable-next-line: one-variable-per-declaration
const environment: object = {
	production: true
};

export { glue, debug, environment };
