const glue = {
	"restserver": {
		"endpoint": "http://localhost:3000",
		"api": {
			"version": "v1.0.1"
		}
	}
};

const debug = true;

// tslint:disable-next-line: one-variable-per-declaration
const environment: object = {
	production: false
};

export { glue, debug, environment };
