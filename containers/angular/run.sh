#!/bin/bash

DIR="$(dirname $0)"
source $DIR/vars.sh

docker run \
	--tmpfs /run \
	-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
	--rm \
	--name $CONTAINER_NAME \
	-d \
	-e 80 \
	-p 8001:80 \
	${IMAGE_NAME}:${GIT_REF}
