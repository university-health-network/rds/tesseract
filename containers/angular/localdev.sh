#!/bin/bash

DIR="$(dirname $(readlink -f $0))"
cd $DIR/www

source $DIR/../localdev.glue.env
export WEBSERVER_DOMAIN
export WEBSERVER_PORT
export RESTSERVER_URL
export RESTSERVER_PORT
export ENV

ng serve --live-reload --open
