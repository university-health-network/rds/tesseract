#!/bin/bash

DIR="$(dirname $0)"
source $DIR/vars.sh

docker build -t ${REGISTRY_ENDPOINT}/${IMAGE_NAME} \
	-t ${REGISTRY_ENDPOINT}/${IMAGE_NAME}:$GIT_REF \
	-t ${REGISTRY_ENDPOINT}/${IMAGE_NAME}:$GIT_BRANCH \
	-t ${IMAGE_NAME}:$GIT_REF \
	$PWD
