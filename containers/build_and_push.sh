#!/bin/bash

CDIR=$(git root)/containers
source $CDIR/vars.sh

(cd $CDIR/nodejs; ./build.sh && ./push.sh) && (cd $CDIR/angular; ./build.sh && ./push.sh)
