const payload = [
	{
		"id": "f7e216d6-9056-49c9-854c-12c19fd4d313",
		"class": ["Cell Sorter"],
		"make": "Bio Rad",
		"model": "S3e",
		"label": "MoeDevice"
	},
	{
		"id": "4b0ac738-2b45-4edc-ac98-567e10610229",
		"class": ["Imaging Mass Cyclometer", "Linear Unipolar TOF"],
		"model": "SimulTOF ONE",
		"make": "SimulTOF",
		"outputFormats": [
			'sqlite', 'imzML'
		],
		"label": "CurlyDevice"
	},
	{
		"id": "d4376986-e058-420f-9ee1-2376285e608f",
		"class": ["ECG Machine"],
		"model": "Cardiart 9108D",
		"make": "BPL",
		"outputFormats": [
			'PDF', 'SCP', 'FDA-XML', 'DICOM', 'HL7'
		],
		"label": "LarryDevice"
	}
];
const metadata = {
	"records": {
		"of": 3,
		"from": 0,
		"to": 2
	},
	"schema": {
		"version": [0, 1, 2]
	}
};

export default { "_": metadata, payload };
