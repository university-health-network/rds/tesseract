const payload = [
	{
		"id": "cf63aef6-51c0-4d43-89c5-42f881d9486e",
		"deviceInstanceId": "c6c021e5-df98-464b-b12c-932e95f8b3c7",
		"label": "Larry"
	},
	{
		"id": "414c47fd-0aca-47a4-86a5-dae810d77ebe",
		"deviceInstanceId": "02924523-e709-4491-b9fe-2a6c5327799f",
		"label": "Moe"
	},
	{
		"id": "09f349fc-c56a-4bec-a58a-1f45d0432693",
		"deviceInstanceId": "bb238a2f-1b8a-4eb7-9bd6-c4b8833d33ac",
		"label": "Curly"
	}
];

interface ApiResponse {
	"_": object;
	"payload": object[];
}

const metadata = {
	"from": 0,
	"to": payload.length - 1,
	"of": payload.length
};
const r: ApiResponse = { "_": metadata, payload };

export default r;
