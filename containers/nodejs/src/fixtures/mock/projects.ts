const payload = [
	{
		"id": "98765424-381e-4b3e-848d-b8e09ccc1a57",
		"name": "LabAProject1",
		"description": "A study on the effect of protein CRISPR5 on cats",
		"lab": "Lab A"
	},
	{
		"id": "df097588-d71a-4b91-9135-02c346d2539f",
		"name": "LabAProject2",
		"description": "A study on the effect of protein CRISPR5 on dogs",
		"lab": "Lab A"
	},
	{
		"id": "6c048ac8-53f6-4f08-8151-669579e45a4a",
		"name": "LabBProject1",
		"description": "A study on the effect of hydrochloric acid on worms",
		"lab": "Lab B"
	},
	{
		"id": "2039ff91-c05a-4be3-901e-ec53666a05db",
		"name": "LabBProject2",
		"description": "A randomized trial of weird fruit",
		"lab": "Lab B"
	}
];

interface ApiResponse {
	"_": object;
	"payload": object[];
}

const metadata = {
	"from": 0,
	"to": payload.length - 1,
	"of": payload.length
};
const r: ApiResponse = { "_": metadata, payload };

export default r;
