const payload = [
	{
		"id": "fbc26e6e-fda5-4147-8152-3bc4d37cde84",
		"deviceTypeId": "f7e216d6-9056-49c9-854c-12c19fd4d313",
		"label": "1st floor, Munk Building."
	},
	{
		"id": "c6c021e5-df98-464b-b12c-932e95f8b3c7",
		"deviceTypeId": "f7e216d6-9056-49c9-854c-12c19fd4d313",
		"label": "15th floor, Munk Building, room 15-4A",
		"notes": [
			"Contact Sandy Jacobs: 416-943-5599"
		]
	},
	{
		"id": "bfccc709-bf2f-49d4-b761-6d2bb3e7c719",
		"deviceTypeId": "4b0ac738-2b45-4edc-ac98-567e10610229",
		"label": "2nd Floor, Lunenfeld-Tanenbaum Lab, Mt Sinai",
		"notes": [
			"significant distortion image outputs"
		]
	},
	{
		"id": "02924523-e709-4491-b9fe-2a6c5327799f",
		"deviceTypeId": "4b0ac738-2b45-4edc-ac98-567e10610229",
		"label": "Ol' Rusty - Laboritory Services"
	},
	{
		"id": "9eb935dc-0810-4acc-8254-fc703fe6516b",
		"deviceTypeId": "4b0ac738-2b45-4edc-ac98-567e10610229",
		"label": "2nd Floor, Nuclear Medicine, Women's College, Grenville",
		"notes": [
			"Phone: 416-323-6400 ext.6184",
			"Hours of Service: \nMonday to Friday \n8:30 a.m. to 4:30 p.m.",
			"Nuclear Medicine \nDepartment of Medical Imaging \nWomen's College Hospital \n76 Grenville Street\n2nd floor\nToronto, ON M5S 1B2"
		]
	},
	{
		"id": "bb238a2f-1b8a-4eb7-9bd6-c4b8833d33ac",
		"deviceTypeId": "d4376986-e058-420f-9ee1-2376285e608f",
		"label": "Penthouse, Mt Sinai",
		"notes": [
			"https://www.uhn.ca/LMP/PatientsFamilies/Blood_Labs/Blood_Drawn/Pages/about_us.aspx"
		]
	},
	{
		"id": "3afd4b12-c71e-44ac-91cd-65ae8eb2ea10",
		"deviceTypeId": "d4376986-e058-420f-9ee1-2376285e608f",
		"label": "Toronto Western, McLaughlin - 1st Floor, Room 405 in the Diagnostic Test Centre​​​",

	}
];

interface ApiResponse {
	"_": object;
	"payload": object[];
}

const metadata = {
	"from": 0,
	"to": payload.length - 1,
	"of": payload.length
};
const r: ApiResponse = { "_": metadata, payload };

export default r;
