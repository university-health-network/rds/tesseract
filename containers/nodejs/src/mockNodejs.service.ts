import { Injectable } from '@nestjs/common';

import deviceTypes from './fixtures/mock/device-types';
import deviceInstances from './fixtures/mock/device-instances';
import deviceBuckets from './fixtures/mock/device-buckets';

interface ApiResponse {
	"_": object;
	"payload": object[];
}

@Injectable()
export class Nodejs {
	deviceTypes(): Promise<ApiResponse> {
		return Promise.resolve(deviceTypes);
	}
	deviceInstances(): Promise<ApiResponse> {
		return Promise.resolve(deviceInstances);
	}
	deviceBuckets(): Promise<ApiResponse> {
		return Promise.resolve(deviceBuckets);
	}
}
