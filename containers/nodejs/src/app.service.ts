import { Injectable } from '@nestjs/common';

const listResources = () => {
	// @todo: make this automatic. Ideally, by using introspection of app.module.ts or some appropriate object
	const officialResources = [
		"/devicebuckets",
		"/deviceinstances",
		"/devicetypes"
	];
	const debugResources = [];
};

@Injectable()
export class AppService {
	getHello(): any {
		return { hello: 'I am the default root' };
	}
	getLong(): any {
		return {
			long: 'I am long',
			lorem: [
				'Lorem ipsum dolor amet wayfarers gluten-free yuccie, bicycle rights pour-over coloring book you probably haven\'t heard of them locavore PBR&B church-key asymmetrical godard. Copper mug air plant poutine actually. Beard single-origin coffee forage hella fanny pack pok pok artisan, umami tumblr. Cred ethical chicharrones DIY, letterpress selvage shoreditch. Letterpress iPhone celiac air plant, crucifix occupy asymmetrical. Small batch viral readymade waistcoat.',
				'Meggings biodiesel kale chips hammock hella aesthetic, pabst tacos heirloom locavore hell of mixtape salvia franzen umami. Next level live-edge franzen hella, pitchfork beard four loko hot chicken shoreditch hell of cardigan put a bird on it. Meh enamel pin gochujang craft beer listicle, yr banh mi succulents selvage trust fund kitsch ugh synth. Kale chips pop-up YOLO skateboard viral flexitarian prism tote bag chartreuse tbh leggings. Jianbing mlkshk plaid raclette.',
				'Kinfolk mlkshk etsy, DIY affogato semiotics quinoa biodiesel meh meggings tattooed. Fixie salvia yuccie venmo actually keytar edison bulb paleo farm-to-table pinterest gentrify you probably haven\'t heard of them mixtape. Tote bag man bun aesthetic bushwick. Asymmetrical knausgaard tattooed tumblr, hoodie umami crucifix vaporware fashion axe messenger bag forage synth adaptogen direct trade.',
			],
		};
	}
	getEnvironment(): any {
		return { environmemnt: process.env };
	}
	getCats(): any {
		return { cats: 'je suis un chat' };
	}
	getHealthy(): any {
		return { healthy: true };
	}
}
