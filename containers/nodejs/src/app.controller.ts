import { Controller, Get, Res, HttpCode } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) { }
	@Get()
	getHello(): string {
		return this.appService.getHello();
	}
	@Get('health')
	getHealthy(): string {
		return this.appService.getHealthy();
	}
	@Get('env')
	getEnvironment(): string {
		return this.appService.getEnvironment();
	}
}
