import { Test, TestingModule } from '@nestjs/testing';
import { DevicebucketsService } from './devicebuckets.service';

describe('DevicebucketsService', () => {
  let service: DevicebucketsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DevicebucketsService],
    }).compile();

    service = module.get<DevicebucketsService>(DevicebucketsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
