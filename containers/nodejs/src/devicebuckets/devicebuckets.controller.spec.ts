import { Test, TestingModule } from '@nestjs/testing';
import { DevicebucketsController } from './devicebuckets.controller';

describe('Devicebuckets Controller', () => {
  let controller: DevicebucketsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DevicebucketsController],
    }).compile();

    controller = module.get<DevicebucketsController>(DevicebucketsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
