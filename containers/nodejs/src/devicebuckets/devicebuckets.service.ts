import { Injectable } from '@nestjs/common';
import { Nodejs } from '../mockNodejs.service';

@Injectable()
export class DeviceBucketsService {
	constructor(private nodejs: Nodejs) { }
	findAll(): any {
		return this.nodejs.deviceBuckets();
	}
}
