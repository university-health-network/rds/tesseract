import { Controller, Get } from '@nestjs/common';
import { DeviceBucketsService } from './devicebuckets.service';

interface ApiResponse {
	"_": object;
	"payload": object[];
}

@Controller('device-buckets')
export class DeviceBucketsController {
	constructor(private service: DeviceBucketsService) { }
	@Get()
	findAll(): Promise<ApiResponse> {
		return this.service.findAll();
	}
}
