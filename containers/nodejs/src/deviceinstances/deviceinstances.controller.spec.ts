import { Test, TestingModule } from '@nestjs/testing';
import { DeviceinstancesController } from './deviceinstances.controller';

describe('Deviceinstances Controller', () => {
  let controller: DeviceinstancesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DeviceinstancesController],
    }).compile();

    controller = module.get<DeviceinstancesController>(DeviceinstancesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
