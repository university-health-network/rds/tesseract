import { Controller, Get } from '@nestjs/common';
import { DeviceinstancesService } from './deviceinstances.service';

interface ApiResponse {
	"_": object;
	"payload": object[];
}

@Controller('device-instances')
export class DeviceinstancesController {
	constructor(private service: DeviceinstancesService) { }
	@Get()
	findAll(): Promise<ApiResponse> {
		return this.service.findAll();
	}
}
