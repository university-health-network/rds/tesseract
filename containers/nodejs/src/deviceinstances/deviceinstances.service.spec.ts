import { Test, TestingModule } from '@nestjs/testing';
import { DeviceinstancesService } from './deviceinstances.service';

describe('DeviceinstancesService', () => {
  let service: DeviceinstancesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DeviceinstancesService],
    }).compile();

    service = module.get<DeviceinstancesService>(DeviceinstancesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
