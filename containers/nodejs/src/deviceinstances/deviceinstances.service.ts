import { Injectable } from '@nestjs/common';
import { Nodejs } from '../mockNodejs.service';

@Injectable()
export class DeviceinstancesService {
	constructor(private nodejs: Nodejs) { }
	findAll(): any {
		return this.nodejs.deviceInstances();
	}
}
