import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './cats/cats.controller';
import { DogsController } from './dogs/dogs.controller';
import { DogsService } from './dogs/dogs.service';
import { DevicetypesController } from './devicetypes/devicetypes.controller';
import { DevicetypesService } from './devicetypes/devicetypes.service';
import { Nodejs } from './mockNodejs.service';
import { DeviceinstancesController } from './deviceinstances/deviceinstances.controller';
import { DeviceinstancesService } from './deviceinstances/deviceinstances.service';
import { ProjectsController } from './projects/projects.controller';
import { DeviceBucketsController } from './devicebuckets/devicebuckets.controller';
import { DeviceBucketsService } from './devicebuckets/devicebuckets.service';

@Module({
	imports: [],
	controllers: [AppController, CatsController, DogsController, DevicetypesController, DeviceinstancesController, ProjectsController, DeviceBucketsController],
	providers: [AppService, DogsService, DevicetypesService, Nodejs, DeviceinstancesService, DeviceBucketsService],
})
export class AppModule { }
