import { Controller, Get } from '@nestjs/common';
import { DevicetypesService } from './devicetypes.service';

@Controller('device-types')
export class DevicetypesController {
	constructor(private service: DevicetypesService) { }
	@Get()
	findAll(): string {
		return this.service.findAll();
	}
}
