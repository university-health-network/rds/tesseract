import { Injectable } from '@nestjs/common';
import { Nodejs } from '../mockNodejs.service';

@Injectable()
export class DevicetypesService {
	constructor(private nodejs: Nodejs) { }
	findAll(): any {
		return this.nodejs.deviceTypes();
	}
}
