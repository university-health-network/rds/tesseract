import { Test, TestingModule } from '@nestjs/testing';
import { DevicetypesController } from './devicetypes.controller';

describe('Devicetypes Controller', () => {
	let controller: DevicetypesController;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			controllers: [DevicetypesController],
		}).compile();

		controller = module.get<DevicetypesController>(DevicetypesController);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});
});
