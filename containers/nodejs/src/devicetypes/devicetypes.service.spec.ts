import { Test, TestingModule } from '@nestjs/testing';
import { DevicetypesService } from './devicetypes.service';

describe('DevicetypesService', () => {
  let service: DevicetypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DevicetypesService],
    }).compile();

    service = module.get<DevicetypesService>(DevicetypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
