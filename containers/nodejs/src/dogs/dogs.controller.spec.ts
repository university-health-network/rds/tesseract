import { Test, TestingModule } from '@nestjs/testing';
import { DogsController } from './dogs.controller';
import { DogsService } from './dogs.service';

describe('DogsController', () => {
  let controller: DogsController;
  let service: DogsService;

  beforeEach(() => {
    service = new DogsService();
    controller = new DogsController(service);
  });

  describe('findAll', () => {
    it('should return a promise resolving to 3 dogs', async () => {
      const result = ['labrador', 'poodle', 'wippet_xx'];
      jest.spyOn(service, 'findAll').mockImplementation(() => result);
      expect(await controller.findAll()).toBe(result);
    });
  });
});
