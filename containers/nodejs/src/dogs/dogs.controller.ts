import { Controller, Get } from '@nestjs/common';
import { DogsService } from './dogs.service';

@Controller('dogs')
export class DogsController {
	constructor(private readonly dogService: DogsService) { }
	@Get()
	async findAll(): Promise<any[]> {
		return this.dogService.findAll();
	}
}
