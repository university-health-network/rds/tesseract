import { Injectable } from '@nestjs/common';

@Injectable()
export class DogsService {
	findAll(): any {
		return Promise.resolve(['labrador', 'poodle', 'wippet']);
	}
}
