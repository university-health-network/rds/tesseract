import { Test, TestingModule } from '@nestjs/testing';
import { CatsController } from './cats.controller';
import { AppService } from '../app.service';

describe('CatsController', () => {
  let catsController: CatsController;
  let catsService: AppService;

  beforeEach(() => {
    catsService = new AppService();
    catsController = new CatsController(catsService);
  });

  describe('getCats', () => {
    it('should something for cats', async () => {
      const result = ['test'];
      jest.spyOn(catsService, 'getCats').mockImplementation(() => result);
      expect(await catsController.getCats()).toBe(result);
    });
  });
});
