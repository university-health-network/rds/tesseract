import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
	const konfig = { cors: true };
	const app = await NestFactory.create(AppModule, konfig);
	await app.listen(process.env.RESTSERVER_PORT);
}

bootstrap();
