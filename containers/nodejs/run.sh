#!/bin/bash

DIR="$(dirname $0)"
source $DIR/vars.sh

docker run -d -P ${IMAGE_NAME}:${GIT_REF} && docker ps -l
