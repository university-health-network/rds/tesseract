# Tesseract REST Server

<img src="tesseract.jpg" alt="Tesseract" />

## Description

This is the REST Server that powers the RDS app, Tesseract. It is built with [Nest](https://github.com/nestjs/nest) and TypeScript.

## Requirements
- nodejs
- docker
- yarn

## Installation

```bash
$ yarn
```

## Running the app

```bash
# development - watch mode
$ ./localdev.sh
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

  Nest is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).
