#!/usr/bin/env -u LC_ALL bash

VERSION="v0.1.0"
GIT_REF="$(git rev-parse @)"
GIT_BRANCH="$(git symbolic-ref --short HEAD)"
GIT_REMOTE="git@gitlab.com:university-health-network/rds/tesseract.git"
IMAGE_NAME="university-health-network/rds/tesseract/nodejs"
REGISTRY_ENDPOINT="registry.gitlab.com"
