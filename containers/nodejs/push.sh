#!/bin/bash

DIR="$(dirname $0)"
source $DIR/vars.sh

docker push ${REGISTRY_ENDPOINT}/${IMAGE_NAME}
docker push ${REGISTRY_ENDPOINT}/${IMAGE_NAME}:$GIT_REF
docker push ${REGISTRY_ENDPOINT}/${IMAGE_NAME}:$GIT_BRANCH
