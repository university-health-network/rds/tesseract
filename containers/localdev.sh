#!/bin/bash

#   @project: rds - tesseract
#   open the server and client in a tmux session.
#	create the tmux session `tesseract` if it doesn't exist

S=tesseract
DIR="$(dirname $(readlink -f $0))"
W1=devspace

##	glue
source $DIR/localdev.glue.env

CMD1="$DIR/nodejs/localdev.sh"
CMD2="$DIR/angular/localdev.sh"

[ tmux has-session -t $S 2> /dev/null ] || {
	tmux detach
	tmux new-session -s $S -n $W1 -d $CMD1
	tmux split-window -h $CMD2
	tmux select-layout even-horizontal
}

tmux attach -t $S
