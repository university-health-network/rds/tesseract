#!/bin/bash

DIR="$(git root)/tesseract"
source $DIR/vars.sh

##	@todo: evaluate if we need to prevent deploys when GIT_IS_CLEAN == no

##	fully qualified instance name
FQIN="${ORG}-${PROJECT}-${CHART_NAME}-${GIT_BRANCH}"

helm upgrade \
	--atomic \
	--install \
	--set git.branch=$GIT_BRANCH \
	--set git.ref=$GIT_REF \
	--set git.is_clean=$GIT_IS_CLEAN \
	--set app=$CHART_NAME \
	--set fqin=$FQIN \
	$FQIN $DIR
