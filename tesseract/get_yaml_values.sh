#!/usr/bin/env node

/**
 *	@description: export yaml values as bash environment variables
 **/

var readYaml = require('read-yaml');

const yal = (tuples, fyle, extractor) => {
	return new Promise((resolve,reject) => {
		readYaml(fyle, (err, data) => {
			if (err) {
				reject(err);
			} else {
				tuples.push(extractor(data));
				resolve(tuples);
			}
		});
	});
};

const xal = (tuples, fyle, extractor) => {
	return yal.bind(null, tuples, fyle, extractor);
};


const chartName = (yml) => {
	const tup = [ "CHART_NAME", yml.name ];
	return tup;
};

const print = (tuples) => {;
	tuples.forEach(row => {
		let bashRow = `${row[0]}="${row[1]}"`;
		console.log(bashRow);
	});
	return Promise.resolve(true);
};

const yuk = err => { console.error(err); return false; };

yal([], 'Chart.yaml', chartName)
	.then(tuples => {
		return yal(tuples,'Chart.yaml', data => {
			return [ "CHART_VERSION", data.version ];
		});
	})
	.then(tuples => {
		return yal(tuples, 'values.yaml', data => ["PROJECT", data.project]);
	})
	.then(tuples => {
		return yal(tuples, 'values.yaml', data => ["ORG", data.org]);
	})
	.then(tuples => yal(tuples, 'values.yaml', data => ["DOMAIN", data.domain]))
	.then(tuples => yal(tuples, 'values.yaml', data => ["NODEJS_REPO", data.nodejs.repository]))
	.then(print)
	.catch(yuk);

