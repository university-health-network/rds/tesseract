#!/bin/bash

GIT_REF="$(git rev-parse @)"
GIT_BRANCH="$(git symbolic-ref --short HEAD)"
GIT_IS_CLEAN="$(git diff-index --quiet HEAD && echo yes || echo no)"
eval $(cd $DIR; ./get_yaml_values.sh)
