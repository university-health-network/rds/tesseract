# tesseract

## A techna-flavoured helm chart, for k8s.

This subdirectory is all about helm. And helm is all about wrangling different instances of the app, as relates to kubernetes.

Helm manages deploying the app because it's well-suited to having different variations of the same app.

